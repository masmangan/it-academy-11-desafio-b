import 'package:test/test.dart';
import 'package:greeting_app/hello/greeting.dart';

void main() {
  test('Hello World Salutation', () {
    expect(Greeting().salutation, 'Hello World');
  });
}
